# Book API
## Installing using GitLab:
Install PostgresSQL and create db

1. Clone the repository:

```bash
git clone https://gitlab.com/andriygrab6/test-task
```
2. Change to the project's directory:
```bash
cd test-task
```
3. Сopy .env_sample file with your creds of env variables to your .env
file

4. Once you're in the desired directory, run the following command to create a virtual environment:
```bash
python -m venv venv
```
5. Activate the virtual environment:

On macOS and Linux:

```bash
source venv/bin/activate
```
On Windows:
```bash
venv\Scripts\activate
```

4. Install the dependencies

```bash
pip install -r requirements.txt
```

5. Set up the database:

Run the migrations

```bash
alembic upgrade head
```

6. Start the development server

```bash
python app.py
```

7. Access the website locally at http://localhost:8080.

## Run with Docker

Docker should be installed

```bash
docker-compose up --build
```

## Endpoints

 
- /books/ (GET, POST) - create/read books
- /books/{book_id}/ (GET) - read book
- /parse-excel/ (POST) - upgrade denied book list


