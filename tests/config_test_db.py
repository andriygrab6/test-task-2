import sqlite3
import os

# Define the path to your SQLite test database file
TEST_DB_PATH = "test_db.sqlite"


def initialize_test_db():
    """Initialize the SQLite test database."""
    conn = sqlite3.connect(TEST_DB_PATH)
    cursor = conn.cursor()

    cursor.execute(
        """
    CREATE TABLE IF NOT EXISTS books (
        id INTEGER PRIMARY KEY,
        name TEXT,
        date_published DATE,
        author TEXT,
        genre TEXT,
        url TEXT,
        in_denied_list BOOL
    )
    """
    )

    conn.commit()
    conn.close()


def clear_test_db():
    """Clear all data from the SQLite test database."""
    if os.path.exists(TEST_DB_PATH):
        os.remove(TEST_DB_PATH)
