from datetime import datetime
from json import dumps

import aiohttp
import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app import create_app
from db.models import Book
from tests.config_test_db import initialize_test_db, clear_test_db

TEST_DB_URL = "sqlite:///test_db.sqlite"


@pytest.fixture(scope="module", autouse=True)
def setup_test_db():
    initialize_test_db()
    yield
    clear_test_db()


@pytest.fixture(scope="module", autouse=True)
def Session():
    engine = create_engine(TEST_DB_URL, echo=True)
    session = sessionmaker(bind=engine)
    return session


@pytest.fixture
def server(loop, aiohttp_server, monkeypatch):
    app = create_app()
    return loop.run_until_complete(aiohttp_server(app))


@pytest.fixture
def client(loop, server, aiohttp_client, monkeypatch):
    return loop.run_until_complete(aiohttp_client(server))


@pytest.fixture
def sample_books(Session):
    session = Session()
    sample_book1 = Book(
        name="test1",
        genre="test1",
        date_published=datetime.strptime("2023-12-12", "%Y-%m-%d").date(),
        author="test1",
    )
    sample_book2 = Book(
        name="test2",
        genre="test2",
        date_published=datetime.strptime("2023-12-13", "%Y-%m-%d").date(),
        author="test2",
    )
    session.add(sample_book1)
    session.add(sample_book2)
    session.commit()
    return sample_book1, sample_book2


@pytest.mark.asyncio
async def test_get_books(client, server, monkeypatch, Session, sample_books):
    monkeypatch.setattr("services.services.get_session", Session)

    session = Session()
    books = session.query(Book).all()
    books_data = [
        {
            "id": book.id,
            "name": book.name,
            "author": book.author,
            "genre": book.genre,
            "date_published": str(book.date_published),
        }
        for book in books
    ]
    books_data = dumps({"books": books_data})
    resp = await client.get("/books/")
    assert resp.status == 200
    text = await resp.text()
    assert text == books_data


@pytest.mark.asyncio
async def test_get_books_by_name_author_genre_date_created(
    client, server, monkeypatch, Session, sample_books
):
    monkeypatch.setattr("services.services.get_session", Session)

    resp = await client.get("/books/?name=test1&genre=test1&author=test1")
    assert resp.status == 200
    text = await resp.text()
    assert (
        dumps(
            {
                "books": [
                    {
                        "id": 1,
                        "name": "test1",
                        "author": "test1",
                        "genre": "test1",
                        "date_published": "2023-12-12",
                    }
                ]
            }
        )
        == text
    )


@pytest.mark.asyncio
async def test_create_book(client, server, monkeypatch, Session):
    monkeypatch.setattr("services.services.get_session", Session)

    form_data = aiohttp.FormData()
    form_data.add_field("name", "test_name")
    form_data.add_field("author", "test_author")
    form_data.add_field("genre", "test_genre")
    form_data.add_field("date_published", "2023-12-12")

    file = open("test.pdf", "rb")
    form_data.add_field("book", file, filename="your_file_name.pdf")

    resp = await client.post("/books/", data=form_data())
    file.close()
    assert resp.status == 200


@pytest.mark.asyncio
async def test_get_book_by_id(client, server, monkeypatch, Session):
    monkeypatch.setattr("services.services.get_session", Session)

    session = Session()
    sample_book1 = Book(
        name="test1",
        genre="test1",
        date_published=datetime.strptime("2023-12-12", "%Y-%m-%d").date(),
        url="test.pdf",
        author="test1",
    )

    session.add(sample_book1)
    session.commit()

    resp = await client.get("/books/1/")
    assert resp.status == 200
    assert resp.content_type == "application/pdf"


@pytest.mark.asyncio
async def test_get_book_by_id_in_denied_list(client, server, monkeypatch, Session):
    monkeypatch.setattr("services.services.get_session", Session)
    monkeypatch.setattr("app.get_book_html", lambda x: "<html></html>")

    session = Session()
    sample_book1 = Book(
        name="test1",
        genre="test1",
        date_published=datetime.strptime("2023-12-12", "%Y-%m-%d").date(),
        url="test.pdf",
        in_denied_list=True,
        author="test1",
    )

    session.add(sample_book1)
    session.commit()
    resp = await client.get("/books/1/")
    assert resp.status == 200
    assert resp.content_type == "text/html"


@pytest.mark.asyncio
async def test_xlsx_uploading(client, server, monkeypatch, Session):
    monkeypatch.setattr("services.services.get_session", Session)
    monkeypatch.setattr("app.get_book_html", lambda x: "<html></html>")
    form_data = aiohttp.FormData()

    session = Session()
    sample_book1 = Book(
        name="Don quixote",
        genre="test1",
        date_published=datetime.strptime("2023-12-12", "%Y-%m-%d").date(),
        url="test.pdf",
        author="test1",
    )

    session.add(sample_book1)
    session.commit()
    file = open("publisher_decline_list.xlsx", "rb")
    form_data.add_field("file", file)

    resp = await client.post("parse-excel/", data=form_data)
    assert resp.status == 200
    file.close()

    resp = await client.get("/books/1/")
    assert resp.status == 200
    assert resp.content_type == "text/html"
