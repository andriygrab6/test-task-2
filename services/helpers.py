import base64
from io import BytesIO

import openpyxl
from PIL.Image import Image
from pdf2image import convert_from_path


def image_to_base64(image: Image) -> str:
    buffer = BytesIO()
    image.save(buffer, 'JPEG')
    image_bytes = buffer.getvalue()
    image_base64 = base64.b64encode(image_bytes).decode('utf-8')
    return image_base64


def get_book_html(book_url: str) -> str:
    images = convert_from_path(book_url)

    html = '<html>'
    for i, image in enumerate(images):
        image_base64 = image_to_base64(image)
        html += f'<img src="data:image/jpeg;base64,{image_base64}" />'
    html += '</html>'

    return html


def get_names_authors_from_xlsx(temp_file_path: str) -> tuple[list[str], list[str]]:
    workbook = openpyxl.load_workbook(temp_file_path, data_only=True)

    name_sheet = workbook['name']
    authors_sheet = workbook['author']

    names = [str(name) for _, name in name_sheet.iter_rows(min_row=2, values_only=True)]
    authors = [str(author) for _, author in authors_sheet.iter_rows(min_row=2, values_only=True)]

    return names, authors
