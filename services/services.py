import os
from typing import Optional, List, Union, Tuple

from sqlalchemy import func, Date, or_

from db.db import Session
from db.models import Book
from db.schemas import BookCreateSchema


UPLOAD_DIR = "../media"
os.makedirs(UPLOAD_DIR, exist_ok=True)


def get_session():
    return Session()


async def create_book_in_db(book_data: dict) -> Book:
    book_file = book_data.get("book", None)

    book_data = BookCreateSchema(**book_data)
    book_data = book_data.model_dump()

    session = get_session()
    book = Book(**book_data)

    if book_file:
        file_path = os.path.join(UPLOAD_DIR, book.name + book_file.filename)
        with open(file_path, "wb") as f:
            f.write(book_file.file.read())
        book.url = file_path

    session.add(book)
    session.commit()
    session.close()

    return book


async def get_books(name: Optional[str] = None,
                    genre: Optional[str] = None,
                    author: Optional[str] = None,
                    date_published: Optional[Date] = None) -> List[dict]:
    session = get_session()

    query = session.query(Book)
    if name:
        query = query.filter(Book.name == name)
    if genre:
        query = query.filter(Book.genre == genre)
    if author:
        query = query.filter(Book.author == author)
    if date_published:
        query = query.filter(func.cast(Book.date_published, Date) == date_published)

    books = query.all()
    session.close()

    books_data = [
        {
            "id": book.id,
            "name": book.name,
            "author": book.author,
            "genre": book.genre,
            "date_published": str(book.date_published),
        }
        for book in books
    ]

    return books_data


async def get_book_by_id(book_id: int) -> Union[Tuple[str, bool], None]:
    session = get_session()

    book = session.query(Book).filter_by(id=book_id).first()
    session.close()

    if book:
        return book.url, book.in_denied_list

    return (None, None)


def add_books_to_denied_list(names: List[str], authors: List[str]) -> bool:
    try:
        session = get_session()
        books_for_update = (
            session.query(Book)
            .filter(or_(Book.name.in_(names), Book.author.in_(authors)))
            .all()
        )
        for book in books_for_update:
            book.in_denied_list = True

        session.commit()
        session.close()
        return True
    except Exception:
        return False
