from sqlalchemy import Column, Integer, String, Date, Boolean

from db.db import Base


class Book(Base):
    __tablename__ = "books"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    date_published = Column(Date)
    genre = Column(String)
    author = Column(String)
    url = Column(String, nullable=True)
    in_denied_list = Column(Boolean, default=False)
