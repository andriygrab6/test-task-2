from datetime import date
from typing import Optional

from pydantic import BaseModel


class BookCreateSchema(BaseModel):
    name: str
    date_published: date
    genre: str
    author: Optional[str]
