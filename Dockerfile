FROM python:3.10

WORKDIR /app

COPY . /app

RUN apt-get update
RUN apt-get install poppler-utils -y

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

EXPOSE 8080