import os
from typing import Union

import aiofiles
from aiohttp import web
from pydantic import ValidationError

from services.helpers import get_names_authors_from_xlsx, get_book_html
from services.services import (
    create_book_in_db,
    get_books,
    get_book_by_id,
    add_books_to_denied_list,
)


async def create_book(request: web.Request) -> web.Response:
    data = await request.post()

    try:
        await create_book_in_db(data)
    except ValidationError as error:
        return web.json_response({"message": str(error.json())})

    return web.json_response({"message": "book created successfully"})


async def get_book_list(request: web.Request) -> web.Response:
    books_data = await get_books(**request.query)

    if books_data:
        return web.json_response({"books": books_data})
    return web.json_response({"message": "Books not found"})


async def read_book(request: web.Request) -> Union[web.Response, web.FileResponse]:
    book_id = request.match_info["book_id"]
    book_url, in_denied_list = await get_book_by_id(book_id)

    if book_url:
        if in_denied_list:
            html = get_book_html(book_url)
            return web.Response(text=html, content_type="text/html", status=200)

        response = web.FileResponse(book_url, status=200)
        response.content_type = "application/pdf"
        return response

    return web.json_response({"error": "Book not found"}, status=404)


async def parse_excel(request: web.Request) -> web.Response:
    data = await request.post()

    if "file" not in data:
        return web.json_response({"error": "File not provided"}, status=400)

    excel_file = data["file"]
    temp_file_path = os.path.join("media", excel_file.filename)

    async with aiofiles.open(temp_file_path, "wb") as file:
        await file.write(excel_file.file.read())

    names, authors = get_names_authors_from_xlsx(temp_file_path)

    success = add_books_to_denied_list(names, authors)

    os.remove(temp_file_path)
    if success:
        return web.json_response({"message": "Excel file parsed successfully"})
    return web.json_response({"error": "Failed to update books"}, status=400)


def create_app():
    app = web.Application()
    app.router.add_post("/books/", create_book)
    app.router.add_get("/books/", get_book_list)
    app.router.add_get("/books/{book_id}/", read_book)
    app.router.add_post("/parse-excel/", parse_excel)
    return app


if __name__ == "__main__":
    application = create_app()
    web.run_app(application, port=8080)
